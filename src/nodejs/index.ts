const SIZE = 8;

/**
 * Default height of art.
 */
const HEIGHT = SIZE + 1;

/**
 * Default width of art.
 */
const WIDTH = SIZE * 2 + 1;

/**
 * Default symbols used in the art.
 */
const SYMBOLS = " .o+=*BOX@%&#/^";

export type RandomArtData = string | number | number[] | Buffer;

export interface RandomArtDrawOptions {
  /** Should the border be drawn around the iamge */
  showBorder?: boolean;

  /** Should the starting position be changed to startSymbol */
  showStart?: boolean;

  /** Should the ending position be changed to endSymbol */
  showEnd?: boolean;

  /** The symbols used to draw the image, must have a minimum length of 2 */
  symbols?: string;

  /** The symbol used to draw the starting position */
  startSymbol?: string;

  /** The symbol used to draw the ending position */
  endSymbol?: string;
}

export type RandomArtOptions = RandomArtDrawOptions & {
  /** Width of the image, not including any borders */
  width?: number;

  /** Height of the image, not including any borders */
  height?: number;

  /** Encoding of the data if it is a string */
  encoding?: BufferEncoding;
};

/**
 * Converts bytes to ASCII art, just like SSH randomart images.
 * Based on http://www.dirk-loss.de/sshvis/drunken_bishop.pdf
 */
export class RandomArt {
  /** Width of the field */
  #width: number;

  /** Height of the field */
  #height: number;

  /** Field used to calculate the image */
  #field: number[][];

  /** Horizontal center position of the field */
  #centerX: number;

  /** Vertical center of the field */
  #centerY: number;

  /** Current horizontal position in the field */
  #x: number;

  /** Current vertical position in the field */
  #y: number;

  /**
   * @param width Width of the image, not including any borders. Default 17
   * @param height Height of the image, not including any borders. Default 9
   */
  constructor(width = WIDTH, height = HEIGHT) {
    if (width < 1 || !Number.isInteger(width)) {
      throw new Error("width needs to be a positive integer");
    }

    if (height < 1 || !Number.isInteger(height)) {
      throw new Error("height needs to be a positive integer");
    }

    this.#width = width;
    this.#height = height;
    this.#field = [];

    this.#centerX = Math.floor(this.#width / 2);
    this.#centerY = Math.floor(this.#height / 2);
    this.#x = this.#centerX;
    this.#y = this.#centerY;

    // set visited to zero for all positions
    for (let y = 0; y < this.#height; y++) {
      this.#field.push(Array(this.#width).fill(0));
    }
  }

  /** Width of the image, not including any borders */
  get width() {
    return this.#width;
  }

  /** Height of the image, not including any borders */
  get height() {
    return this.#height;
  }

  /**
   * Update the image with new data.
   *
   * @param data Data to update the image with
   * @param encoding If data is a string, use this to decode it into bytes, otherwie ignored
   */
  update(data: RandomArtData, encoding?: BufferEncoding) {
    const steps = this.step(data, encoding);

    while (!steps.next().done) {}

    return this;
  }

  /**
   * Update the image, `yield`ing after each change. This is useful if you want to see step
   * by step how the image changes.
   *
   * @param data Data to update the image with
   * @param encoding If data is a string, use this to decode it into bytes, otherwie ignored
   */
  *step(data: RandomArtData, encoding?: BufferEncoding) {
    if (typeof data === "number") {
      data = [data];
    } else if (typeof data === "string") {
      data = Buffer.from(data, encoding);
    }

    for (let byte of data) {
      // only uses first 8 bits even if the number is bigger than that
      // process two bits at a time
      for (let shift = 0; shift < 4; shift++) {
        // low bit moves left and right when in bounds
        if (byte & 0x1) {
          if (this.#x < this.#width - 1) {
            this.#x++;
          }
        } else if (this.#x > 0) {
          this.#x--;
        }

        // high bit moves up and down when in bounds
        if (byte & 0x2) {
          if (this.#y < this.#height - 1) {
            this.#y++;
          }
        } else if (this.#y > 0) {
          this.#y--;
        }

        this.#field[this.#y][this.#x]++;

        byte >>>= 2;
        yield;
      }
    }
  }

  /**
   * Get the randomart image as a string.
   *
   * @param param0 Options for how the image should look
   */
  draw({
    showBorder = true,
    showStart = true,
    showEnd = true,
    symbols = SYMBOLS,
    startSymbol = "S",
    endSymbol = "E",
  }: RandomArtDrawOptions = {}) {
    let image = "";
    const symbolsLength = symbols.length;

    if (symbolsLength < 2) {
      throw new Error("need at least 2 symbols");
    }

    if (showStart && startSymbol.length !== 1) {
      throw new Error("startSymbol needs to have a length of 1");
    }

    if (showEnd && endSymbol.length !== 1) {
      throw new Error("endSymbol needs to have a length of 1");
    }

    for (const [y, row] of this.#field.entries()) {
      let content = "";

      for (const [x, value] of row.entries()) {
        if (showStart && y === this.#centerY && x === this.#centerX) {
          content += startSymbol;
        } else if (showEnd && y === this.#y && x === this.#x) {
          content += endSymbol;
        } else {
          content += symbols[value < symbolsLength ? value : symbolsLength - 1];
        }
      }

      if (showBorder) {
        // add side borders
        content = `|${content}|`;
      }

      if (y < this.#height - 1) {
        content += "\n";
      }

      image += content;
    }

    if (showBorder) {
      // add top and bottom borders
      const border = `+${"-".repeat(this.#width)}+`;

      image = `${border}\n${image}\n${border}`;
    }

    return image;
  }

  /**
   * Get the randomart image as a string with default options.
   */
  toString() {
    return this.draw();
  }
}

/**
 * Create randomart image from data.
 *
 * @param data Data to convert to randomart image
 * @param options Options to change the output of the image
 */
export function randomArt(data: RandomArtData, options: RandomArtOptions = {}) {
  return new RandomArt(options.width, options.height)
    .update(data, options.encoding)
    .draw(options);
}
