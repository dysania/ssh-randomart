# SSH randomart

Module and CLI tool to convert bytes to ASCII art just like SSH randomart images

## Install

```shell
$ npm install ssh-randomart
```

## Usage

### As a module

```js
const { randomArt } = require("ssh-randomart");
// or
import { randomArt } from "ssh-randomart";

console.log(randomArt("Hello world!"));
console.log(randomArt([0xc0, 0xff, 0xee, 0xca, 0xfe]));
```

### As a CLI

```shell
$ echo -n "Hello, world!" | ssh-randomart
```
