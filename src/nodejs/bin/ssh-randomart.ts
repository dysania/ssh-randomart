#!/usr/bin/env node

import arg from "arg";
import { promises as fs } from "fs";
import path from "path";

import { RandomArt, randomArt, RandomArtOptions } from "..";
import { description, version } from "../package.json";

const spec = {
  "--help": Boolean,
  "--version": Boolean,

  "--file": String,
  "--width": Number,
  "--height": Number,
  "--encoding": String,
  "--symbols": String,
  "--start-symbol": String,
  "--end-symbol": String,
  "--no-border": Boolean,
  "--no-start": Boolean,
  "--no-end": Boolean,
  "--progress": Boolean,
  "--delay": Number,

  "-f": "--file",
  "-w": "--width",
  "-h": "--height",
  "-n": "--encoding",
  "-c": "--symbols",
  "-s": "--start-symbol",
  "-e": "--end-symbol",
  "-B": "--no-border",
  "-S": "--no-start",
  "-E": "--no-end",
  "-p": "--progress",
  "-d": "--delay",
};

let args: arg.Result<typeof spec>;

try {
  args = arg(spec);
} catch (e) {
  console.error(e.message);
  process.exit(1);
}

if (args["--version"]) {
  console.log(`v${version}`);
  process.exit(0);
}

if (args["--help"]) {
  const program = path.basename(process.argv[1]);
  const usage = `${description}

Usage:
  ${program} --help
  ${program} --version
  ${program} [--file FILE] [--width WIDTH] [--height HEIGHT] [--encoding ENCODING] \\
      [--symbols SYMBOLS] [--start-symbol START_SYMBOL] [--end-symbol END_SYMBOL] \\
      [--no-border] [--no-start] [--no-end] \\
      [--progress] [--delay DELAY]

Options:
  --help           Show this help message
  --version        Print the version
  --file, -f       Read from a file instead of stdin
  --width, -w      Width of the image, not including borders, default 17
  --height, -h     Height of the image, not including borders, default 9
  --encoding, -n   Encoding to use for the input, "hex", "base64", "utf8", etc
  --symbols, -c    Characters used to draw the image
  --start-symbol, -s   Character used to draw the starting position
  --end-symbol, -e   Character used to draw the ending position
  --no-border, -B    Hide the border around the image
  --no-start, -S     Don't show the starting symbol
  --no-end, -E       Don't show the ending symbol
  --progress, -p     Print out every step while creating the image
  --deplay, -d       Delay in milliseconds between progress steps, default 17`;

  console.log(usage);
  process.exit(0);
}

let data: Promise<Buffer>;

if (args["--file"]) {
  data = fs.readFile(args["--file"]);
} else {
  data = new Promise((resolve, reject) => {
    const buffers: Buffer[] = [];

    process.stdin.on("data", (buffer) => {
      buffers.push(buffer);
    });

    process.stdin.on("error", reject);

    process.stdin.on("end", () => {
      resolve(Buffer.concat(buffers));
    });
  });
}

data
  .then(async function main(buffer) {
    const options: RandomArtOptions = {};

    if (typeof args["--width"] !== "undefined") {
      options.width = args["--width"];
    }

    if (typeof args["--height"] !== "undefined") {
      options.height = args["--height"];
    }

    if (typeof args["--encoding"] !== "undefined") {
      const encoding = args["--encoding"];
      if (!Buffer.isEncoding(encoding)) {
        throw Error(`Not a valid encoding: ${encoding}`);
      }

      options.encoding = encoding;
    }

    if (typeof args["--symbols"] !== "undefined") {
      options.symbols = args["--symbols"];
    }

    if (typeof args["--start-symbol"] !== "undefined") {
      options.startSymbol = args["--start-symbol"];
    }

    if (typeof args["--end-symbol"] !== "undefined") {
      options.endSymbol = args["--end-symbol"];
    }

    if (args["--no-border"]) {
      options.showBorder = false;
    }

    if (args["--no-start"]) {
      options.showStart = false;
    }

    if (args["--no-end"]) {
      options.showEnd = false;
    }

    if (options.encoding) {
      // handle encoding here instead of the API in order to keep the data a buffer
      // this is helpful because in the --progress option it loops over bytes
      buffer = Buffer.from(buffer.toString("utf8"), options.encoding);
      delete options.encoding;
    }

    if (!args["--progress"]) {
      console.log(randomArt(buffer, options));
      process.exit(0);
    }

    /*
     * 1) Draw image
     * 2) wait delay
     * 3) move cursor up to top of image
     * 4) goto 1
     *
     * Wait _before_ moving the cursor so that most of the time the cursor is
     * at the bottom and if the user quits then it will be in a good place.
     * Alternatively the user would quit with the cursor at the top of the image
     * and it would be ugly to have the prompt show up in the middle of the
     * screen.
     */

    const delay = typeof args["--delay"] !== "undefined" ? args["--delay"] : 17;
    const art = new RandomArt(options.width, options.height);

    // draw initial empty image
    console.log(art.draw(options));

    for (const byte of buffer) {
      const steps = art.step(byte);

      while (!steps.next().done) {
        await sleep(delay);

        const image = art.draw(options);
        const numLines = image.split("\n").length;

        // move cursor to beginning of line and up to the top of the image to overwrite it
        // using ANSI escape code \e[nA to move up n lines
        process.stdout.write(`\r\x1b[${numLines}A`);

        console.log(image);
      }
    }
  })
  .catch((e) => {
    console.error(e.message);
    process.exit(1);
  });

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
